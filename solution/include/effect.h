#ifndef EFFECT_H
#define EFFECT_H

#include "image.h"

#include <stdint.h>

enum rotate_status {
  EFFECT_OK = 0,
  UNSUPPORTED_ARG,
};

enum rotate_status rotate(struct image *img, int32_t angle);

#endif
