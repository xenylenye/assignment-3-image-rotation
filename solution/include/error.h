#ifndef ERROR_CODES_H
#define ERROR_CODES_H

#include "effect.h"
#include "status.h"

enum error {
  PARAM_ERR = 0,
  VALUE_ERROR,
  FILE_ERROR,
  MEMORY_ERROR,
  READ_ERROR,
  WRITE_ERROR,
};

void exit_with_msg(char *msg, enum error e);

char *get_read_error_msg(enum read_status status);
char *get_rotate_error_msg(enum rotate_status result);
char *get_write_error_msg(enum write_status result);

#endif
