
#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#if defined _MSC_VER
#define __attribute__()
#endif
#pragma pack(push, 1)
struct __attribute__((packed))
 pixel {
  int8_t r, g, b;
};

#pragma pack(pop)

struct image {
  uint64_t width, height;
  struct pixel *data;
};

struct pixel pixel_new(int8_t const r, int8_t const g, int8_t const b);

struct image image_new(uint64_t const width, uint64_t const height);

void image_free(struct image *img);

#endif
