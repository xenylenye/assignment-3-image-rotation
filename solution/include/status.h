#ifndef STATUS_H
#define STATUS_H

enum write_status { WRITE_OK = 0, WRITE_NOT_OK };

enum read_status {
  READ_OK = 0,
  READ_INVALID_HEADER,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS
};

#endif
