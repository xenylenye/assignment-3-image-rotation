#include "bmp.h"
#include "error.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RED_OFFSET 2
#define GREEN_OFFSET 1
#define BLUE_OFFSET 0

#define HANDLE_ERROR_AND_CLEANUP(image_data, error_code)                       \
  do {                                                                         \
    free(image_data);                                                          \
    (image_data) = NULL;                                                       \
    return((error_code));                                                        \
  } while (0)

#define ALLOCATE_MEMORY_AND_CHECK(ptr, size, error_code)                       \
  do {                                                                         \
    (ptr) = malloc(size);                                                      \
    if (!(ptr)) {                                                              \
      return(error_code);                                                        \
    }                                                                          \
  } while (0)

static uint8_t calc_padding(uint64_t const width) {
  return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

static void copy_pixel_data(int8_t *src, struct pixel *dest) {
  dest->r = src[RED_OFFSET];
  dest->g = src[GREEN_OFFSET];
  dest->b = src[BLUE_OFFSET];
}

static int8_t *get_pixel_start(int8_t *src, uint32_t i, uint32_t j,
                               uint32_t width, uint8_t padding) {
  return src + i * (sizeof(struct pixel) * width + padding) +
         j * sizeof(struct pixel);
}

static void copy_row_data(int8_t *src, struct pixel *dest, uint32_t width,
                          uint8_t padding) {
  for (size_t j = 0; j < width; ++j) {
    int8_t *pixel_start = get_pixel_start(src, 0, j, width, padding);
    copy_pixel_data(pixel_start, &dest[j]);
  }
}

static void copy_image_data(int8_t *src, struct image *img,
                            const struct bmp_header *header) {
  uint8_t padding = calc_padding(header->biWidth);

  for (size_t i = 0; i < header->biHeight; ++i) {
    int8_t *row_start = get_pixel_start(src, i, 0, header->biWidth, padding);
    struct pixel *row_dest = &img->data[i * header->biWidth];
    copy_row_data(row_start, row_dest, header->biWidth, padding);
  }
}

enum read_status from_bmp(FILE *in, struct image *img) {
  struct bmp_header bmpHeader = {0};

  if (fread(&bmpHeader, sizeof(struct bmp_header), 1, in) == 0) {
    return READ_INVALID_HEADER;
  }

  if (bmpHeader.bfType != BMP_FILE_TYPE) {
    return READ_INVALID_SIGNATURE;
  }

  fseek(in, bmpHeader.bOffBits, SEEK_SET);

  int8_t *image_data = NULL;

  ALLOCATE_MEMORY_AND_CHECK(image_data, bmpHeader.biSizeImage,
                            READ_INVALID_BITS);

  if (fread(image_data, bmpHeader.biSizeImage, 1, in) < 1) {
    HANDLE_ERROR_AND_CLEANUP(image_data, READ_INVALID_BITS);
  }

  *img = image_new(bmpHeader.biWidth, bmpHeader.biHeight);
  copy_image_data(image_data, img, &bmpHeader);

  free(image_data);
  image_data = NULL;
  return READ_OK;
}

static void copy_pixel_to_data(const struct pixel *src, int8_t *dest) {
  *dest = src->b;
  *(dest + 1) = src->g;
  *(dest + 2) = src->r;
}

static void copy_row_pixels(int8_t *source_data, struct image *img,
                            const struct bmp_header *header, uint64_t i) {
  uint32_t row_start = i * (sizeof(struct pixel) * header->biWidth +
                            calc_padding(header->biWidth));

  for (size_t j = 0; j < img->width; ++j) {
    uint32_t index = i * img->width + j;
    int8_t *pixel_start = source_data + row_start + j * sizeof(struct pixel);
    struct pixel current_pixel = img->data[index];
    copy_pixel_to_data(&current_pixel, pixel_start);
  }
}

static void add_padding(int8_t *source_data, struct image *img, uint64_t i,
                        uint8_t padding) {
  uint32_t row_start = i * (sizeof(struct pixel) * img->width + padding);

  for (size_t j = 0; j < padding; ++j) {
    source_data[row_start + img->width * 3 + j] = 0;
  }
}

static void copy_data_to_image(int8_t *source_data, struct image *img,
                               const struct bmp_header *header) {
  uint8_t padding = calc_padding(header->biWidth);

  for (size_t i = 0; i < img->height; ++i) {
    copy_row_pixels(source_data, img, header, i);
    add_padding(source_data, img, i, padding);
  }
}

static void initialize_bmp_header(struct bmp_header *header, uint32_t image_size, uint32_t width,
                           uint32_t height) {
  header->bfType = BMP_FILE_TYPE;
  header->bfileSize = image_size + sizeof(struct bmp_header);
  header->bfReserved = 0;
  header->bOffBits = sizeof(struct bmp_header);
  header->biSize = SIZE;
  header->biWidth = width;
  header->biHeight = height;
  header->biPlanes = PLANES;
  header->biBitCount = BIT_COUNT;
  header->biCompression = COMPRESSION;
  header->biSizeImage = image_size;
  header->biXPelsPerMeter = X_PELS_PER_METERS;
  header->biYPelsPerMeter = Y_PELS_PER_METERS;
  header->biClrUsed = CLR_USED;
  header->biClrImportant = CLR_IMPORTANT;
}

enum write_status to_bmp(FILE *out, struct image *img) {
  uint32_t image_size =
      (img->width * sizeof(struct pixel) + calc_padding(img->width)) *
      img->height;

  struct bmp_header header = {0};

  initialize_bmp_header(&header, image_size, img->width, img->height);

  if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) {
    return WRITE_NOT_OK;
  }

  int8_t *image_data = NULL;

  ALLOCATE_MEMORY_AND_CHECK(image_data, header.biSizeImage, WRITE_NOT_OK);

  copy_data_to_image(image_data, img, &header);

  if (fwrite(image_data, header.biSizeImage, 1, out) < 1) {
    HANDLE_ERROR_AND_CLEANUP(image_data, WRITE_NOT_OK);
  }

  free(image_data);
  image_data = NULL;
  return WRITE_OK;
}
