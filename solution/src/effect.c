#include "effect.h"
#include <stdio.h>

// 分支预测离开聊天室
// 希望跳表 PLSPLSPLSPLS

static enum rotate_status rotate_image(struct image *img, int32_t angle,
                                  uint32_t const new_width,
                                  uint32_t const new_height) {
  struct image copy = image_new(new_width, new_height);

  for (size_t i = 0; i < copy.height; ++i) {
    for (size_t j = 0; j < copy.width; ++j) {

      uint32_t old_x = 0;
      uint32_t old_y = 0;

      switch (angle) {
      case -270:
      case 90:
        old_x = copy.height - 1 - i;
        old_y = j;
        break;
      case -180:
      case 180:
        old_x = copy.width - 1 - j;
        old_y = copy.height - 1 - i;
        break;
      case -90:
      case 270:
        old_x = i;
        old_y = copy.width - 1 - j;
        break;
      }

      copy.data[i * copy.width + j] = img->data[old_y * img->width + old_x];
    }
  };

  image_free(img);
  *img = copy;

  return EFFECT_OK;
}

// 希望跳表

enum rotate_status rotate(struct image *img, int32_t angle) {
  angle = angle % 360;

  uint64_t new_width, new_height;
  switch (angle) {
  case 0:
    new_width = img->width;
    new_height = img->height;
    break;
  case 90:
  case -270:
    new_width = img->height;
    new_height = img->width;
    break;
  case 180:
  case -180:
    new_width = img->width;
    new_height = img->height;
    break;
  case 270:
  case -90:
    new_width = img->height;
    new_height = img->width;
    break;
  default:
    return UNSUPPORTED_ARG;
  }

  return rotate_image(img, angle, new_width, new_height);
}
