#include "error.h"
#include <stdio.h>
#include <stdlib.h>

char *get_read_error_msg(enum read_status status) {
  switch (status) {
  case READ_INVALID_SIGNATURE:
    return "Read invalid format";
  case READ_INVALID_HEADER:
    return "Invalid header!";
  case READ_INVALID_BITS:
    return "Invalid data!";
  default:
    return "Unknown read error";
  }
}

char *get_rotate_error_msg(enum rotate_status result) {
  switch (result) {
  case UNSUPPORTED_ARG:
    return "Invalid angle!";
  default:
    return "Unknown rotate error";
  }
}

char *get_write_error_msg(enum write_status result) {
  switch (result) {
  case WRITE_NOT_OK:
    return "Cannot write to target file!";
  default:
    return "Unknown write error";
  }
}

void exit_with_msg(char *msg, enum error e) {
  fprintf(stderr, "%s", msg);
  exit(e);
}
