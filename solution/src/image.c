#include "image.h"
#include "error.h"

#include <stdlib.h>

struct pixel pixel_new(int8_t r, int8_t g, int8_t b) { return (struct pixel){r, g, b}; }

struct image image_new(const uint64_t width, const uint64_t height) {
  struct pixel *data = malloc(width * height * sizeof(struct pixel));

  if (!data) {
    data = NULL;
    exit(MEMORY_ERROR);
  };
  
  return (struct image){width, height, data};
}

void image_free(struct image *img) {
  if (img != NULL) {
    free(img->data);
    img->data = NULL;
  };
}
