#include "bmp.h"
#include "effect.h"
#include "error.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HANDLE_ERROR(err_code, msg)                                            \
  do {                                                                         \
    exit_with_msg((msg), (err_code));                                          \
  } while (0)

#define HANDLE_FILE_ERROR(file, err_code, msg)                                 \
  do {                                                                         \
    fclose(file);                                                              \
    HANDLE_ERROR((err_code), (msg));                                           \
  } while (0)

#define HANDLE_READ_ERROR(status)                                              \
  do {                                                                         \
    HANDLE_ERROR(READ_ERROR, get_read_error_msg(status));                      \
  } while (0)

#define HANDLE_ROTATE_ERROR(result)                                            \
  do {                                                                         \
    HANDLE_ERROR(VALUE_ERROR, get_rotate_error_msg(result));                   \
  } while (0)

#define HANDLE_WRITE_ERROR(result)                                             \
  do {                                                                         \
    HANDLE_ERROR(WRITE_ERROR, get_write_error_msg(result));                    \
  } while (0)

#define ARGS 3

static int do_bmp(FILE *f_input, FILE *f_output, int32_t angle) {
  struct image img = {0};

  if (from_bmp(f_input, &img) != READ_OK) {
    fclose(f_input);
    fclose(f_output);
    HANDLE_READ_ERROR(from_bmp(f_input, &img));
  }

  fclose(f_input);

  if (angle == 0) {
    goto zero_angle;
  }

  if (rotate(&img, angle) != EFFECT_OK) {
    HANDLE_FILE_ERROR(f_output, VALUE_ERROR,
                      get_rotate_error_msg(rotate(&img, angle)));
  }

zero_angle : {

  if (to_bmp(f_output, &img) != WRITE_OK) {
    HANDLE_FILE_ERROR(f_output, WRITE_ERROR,
                      get_write_error_msg(to_bmp(f_output, &img)));
  }

  fclose(f_output);
};

  image_free(&img);
  printf("Rotation completed!");
  return 0;
}

int main(int argc, char **argv) {

  if (argc != (ARGS + 1)) {
    HANDLE_ERROR(PARAM_ERR, "Args were not provided correctly");
  }

  int32_t angle = atoi(argv[3]);

  if (!angle && strcmp(argv[3], "0") != 0) {
    HANDLE_ERROR(PARAM_ERR, "Invalid angle");
  }

  FILE *f_input = fopen(argv[1], "rb");
  FILE *f_output = fopen(argv[2], "wb");

  if (!f_output)
    HANDLE_FILE_ERROR(f_input, FILE_ERROR,
                      "Result file cannot be created or/and opened");

  if (!f_input)
    HANDLE_FILE_ERROR(f_output, FILE_ERROR,
                      "Source file cannot be read or/and opened");

  return do_bmp(f_input, f_output, angle);
}
